#!/bin/bash
# Executed at image build, inspired on elico-odoo image install script
SCRIPT_NAME="Odoo Docker Image Build"
SCRIPT_DESCRIPTION="Installs dependencies and build path"
SCRIPT_AUTHOR="Benoit Lavorata"

###############
### HELPERS ###
###############
# COLORS
__NC=$(echo -en '\033[0m')
__RED=$(echo -en '\033[00;31m')
__GREEN=$(echo -en '\033[00;32m')
__YELLOW=$(echo -en '\033[00;33m')
__BLUE=$(echo -en '\033[00;34m')
__MAGENTA=$(echo -en '\033[00;35m')
__PURPLE=$(echo -en '\033[00;35m')
__CYAN=$(echo -en '\033[00;36m')
__LIGHTGRAY=$(echo -en '\033[00;37m')
__LRED=$(echo -en '\033[01;31m')
__LGREEN=$(echo -en '\033[01;32m')
__LYELLOW=$(echo -en '\033[01;33m')
__LBLUE=$(echo -en '\033[01;34m')
__LMAGENTA=$(echo -en '\033[01;35m')
__LPURPLE=$(echo -en '\033[01;35m')
__LCYAN=$(echo -en '\033[01;36m')
__WHITE=$(echo -en '\033[01;37m')
__GREY=$(echo -en '\033[01;30m')

# SET FORMATS
__COLOR_DATE=$__BLUE
__COLOR_INTRO=$__BLUE
__COLOR_SUB_INTRO=$__YELLOW
__COLOR_INFO=$__BLUE
__COLOR_DEBUG=$__GREY
__COLOR_ERROR=$__RED
__COLOR_SUCCESS=$__GREEN
__COLOR_SECTION=$__WHITE
__COLOR_LOG=$__LIGHTGRAY
__COLOR_PROMPT=$__YELLOW
__COLOR_PROMPT_ANSWER=$__LCYAN

# DEFINE LOG FUNCTION
__DATE='date +%Y%m%d-%H%M%S'
__YEAR=`date +%Y`
__PREFIX=$(echo -en $__COLOR_DATE`$__DATE`" | "$__NC)
__INDENT="  "
__LINE="-------------------------------------------------"

function _intro {
    echo -e "${__COLOR_INTRO}${__LINE}"
    echo -e "SCRIPT:${__COLOR_LOG} ${SCRIPT_NAME} ${__COLOR_INTRO}"
    echo -e "INFO:${__COLOR_LOG} ${SCRIPT_DESCRIPTION}${__COLOR_INTRO}"
    echo -e "AUTHOR:${__COLOR_LOG} ${SCRIPT_AUTHOR}, ${__YEAR} ${__COLOR_INTRO}"
    echo -e "${__LINE}${__NC}"
}

function _outro {
    echo -e "${__COLOR_INTRO}"
    echo -e ${__LINE}
    echo -e "Have a good day,"
    echo -e "$SCRIPT_AUTHOR."
    echo -e ${__LINE}
    echo -e "${__NC}"
}

function _log {
    echo -e ${__PREFIX}${__COLOR_LOG}"$1"${__NC}
}
function _log1 {
    _log "${__INDENT}$1"
}
function _log2 {
    _log1 "${__INDENT}$1"
}
function _log3 {
    _log2 "${__INDENT}$1"
}
function _log4 {
    _log3 "${__INDENT}$1"
}


##########################
### BUSINESS FUNCTIONS ###
##########################
function _purge {
    _log '_purge'
    set -eo pipefail
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false -o APT::AutoRemove::SuggestsImportant=false
    rm -rf /var/lib/apt/lists/* /root/.cache/pip/*
}

function _scan_dir_for_modules_and_install_requirements {
    _log "_scan_dir_for_modules_and_install_requirements "

    DIR_TO_SCAN=$1
    INSTALL_OCA_DEPS=$2
    MAIN_OCA_DEPENDENCIES_FILE=$3
    MAIN_REQUIREMENTS_FILE=$4
    MAIN_PATH_FILE=$5

    #_log1 "DIR_TO_SCAN = $DIR_TO_SCAN"
    #_log1 "INSTALL_OCA_DEPS = $INSTALL_OCA_DEPS"
    #_log1 "MAIN_OCA_DEPENDENCIES_FILE = $MAIN_OCA_DEPENDENCIES_FILE"
    #_log1 "MAIN_REQUIREMENTS_FILE = $MAIN_REQUIREMENTS_FILE"

    cd $DIR_TO_SCAN
    for dir in ./*/
    do
        dir=${dir%*/}
        CURRENT_DIR=${dir##*/}
        _log1 "CURRENT_DIR = $DIR_TO_SCAN/$CURRENT_DIR"

        if [[ -d $DIR_TO_SCAN/$CURRENT_DIR ]]; then
            #echo "$DIR_TO_SCAN/$CURRENT_DIR is a directory"

            cd $DIR_TO_SCAN/$CURRENT_DIR

            if [ "$INSTALL_OCA_DEPS" == "true" ]; then
                FILE="$DIR_TO_SCAN/$CURRENT_DIR/oca_dependencies.txt"
                if [ -f $FILE ]; then
                    #_log1 "  $FILE exists, concat"
                    #cat $FILE
                    echo "" >> $MAIN_OCA_DEPENDENCIES_FILE
                    cat $FILE >> $MAIN_OCA_DEPENDENCIES_FILE
                fi
            fi
            
            FILE="$DIR_TO_SCAN/$CURRENT_DIR/requirements.txt"
            if [ -f $FILE ]; then
                #_log1 "  $FILE exists, concat"
                #cat $FILE
                echo "" >> $MAIN_REQUIREMENTS_FILE
                cat $FILE >> $MAIN_REQUIREMENTS_FILE
            fi
            
            FILE="$DIR_TO_SCAN/$CURRENT_DIR/__init__.py"
            if [ -f $FILE ]; then
                _log2 "$FILE exists, is module , skip recursive..."
            else
                if [ -z $MAIN_PATH_FILE ]; then
                    _log2 "MAIN_PATH_FILE not specified, skip writing path..."
                else
                    _log2 "Write $DIR_TO_SCAN/$CURRENT_DIR to $MAIN_PATH_FILE"
                    echo $DIR_TO_SCAN/$CURRENT_DIR >> $MAIN_PATH_FILE
                fi
                _log2 "$FILE does not exist, is a repo , go recursive +1 to $DIR_TO_SCAN/$CURRENT_DIR"
                for subdir in ./*/
                do
                    subdir=${subdir%*/}
                    SUB_DIR=${subdir##*/}
                    if [[ -d $DIR_TO_SCAN/$CURRENT_DIR/$SUB_DIR ]]; then
                        _log3 "SUB_DIR = $DIR_TO_SCAN/$CURRENT_DIR/$SUB_DIR"
                        
                        cd $DIR_TO_SCAN/$CURRENT_DIR/$SUB_DIR

                        if [ "$INSTALL_OCA_DEPS" == "true" ]; then
                            FILE="$DIR_TO_SCAN/$CURRENT_DIR/$SUB_DIR/oca_dependencies.txt"
                            if [ -f $FILE ]; then
                                #_log1 "    $FILE exists, concat"
                                #cat $FILE
                                echo "" >> $MAIN_OCA_DEPENDENCIES_FILE
                                cat $FILE >> $MAIN_OCA_DEPENDENCIES_FILE
                            fi
                        fi
                        
                        FILE="$DIR_TO_SCAN/$CURRENT_DIR/$SUB_DIR/requirements.txt"
                        if [ -f $FILE ]; then
                            #_log1 "    $FILE exists, concat"
                            #cat $FILE
                            echo "" >> $MAIN_REQUIREMENTS_FILE
                            cat $FILE >> $MAIN_REQUIREMENTS_FILE
                        fi
                        
                        cd $DIR_TO_SCAN/$CURRENT_DIR
                    fi
                done
            fi

            cd $DIR_TO_SCAN
        fi
    done
}

function _download_module {
    _MODULE_NAME=$1
    _VERSION=$2
    _UNZIP=$3
    _UNZIP_PATH=$4

    _log3 "_download_module: $_MODULE_NAME" 
    TARGET="https://apps.odoo.com/apps/modules/${_VERSION}.0/${_MODULE_NAME}/"
    _log4 " Odoo Apps URL: $TARGET"
    
    URL=$(wget -qO- $TARGET |
    grep -Eoi '<a [^>]+>' | 
    grep -Eo 'href="[^\"]+"' | 
    grep 'download' | 
    grep '.zip' |
    grep 'https' |
    grep -Eo '(http|https)://[^"]+')
    _log4 " Zip URL: $URL"
    
    _log4 " Download to: ${_MODULE_NAME}.zip"
    curl $URL -o ${_MODULE_NAME}.zip
    
    if [ -z "$_UNZIP" ]
    then
          _log4 "Skip unzip"
    else
        _log4 "Move ${_MODULE_NAME}.zip to ${_UNZIP_PATH}/${_MODULE_NAME}.zip"
        mv ${_MODULE_NAME}.zip ${_UNZIP_PATH}/${_MODULE_NAME}.zip
        
        _log4 "Unzip ${_UNZIP_PATH}/${_MODULE_NAME}.zip to ${_UNZIP_PATH}/${_MODULE_NAME}"
        cd ${_UNZIP_PATH}
        unzip -qq ${_MODULE_NAME}.zip
        
        #_log3 "Unzipped content: "
        #ls ${_UNZIP_PATH}/${_MODULE_NAME}
        
        _log4 "Remove ${_UNZIP_PATH}/${_MODULE_NAME}.zip"
        rm ${_MODULE_NAME}.zip
        cd $PWD
    fi
}

function _download_marketplace_modules {
    _log1 "_download_marketplace_modules "
    DEST_PATH=$1
    MODULES_LIST_PATH="$DEST_PATH/modules.txt"

    _log2 "Read $MODULES_LIST_PATH"
    while IFS= read -r module
    do
        _download_module $module "12" "unzip" $DEST_PATH
    done < "$MODULES_LIST_PATH"
}

function _build {
    _log "_build"

    DEPS_FILE="/odoo/submodules-src/oca_dependencies.txt"
    REQUIREMENTS_FILE="/odoo/submodules-src/requirements.txt"
    PATH_FILE="/odoo/.addons_path"
    touch $DEPS_FILE
    touch $REQUIREMENTS_FILE
    touch $PATH_FILE

    _log1 'updgrade pip'
    pip install --upgrade pip

    _log1 'install pip deps from /odoo/community-requirements.txt '
    pip install -r /odoo/community_requirements.txt

    _log1 'concatenate requirements and deps for /odoo/submodules-src'
    echo "/odoo/submodules-src" >> $PATH_FILE
    _scan_dir_for_modules_and_install_requirements "/odoo/submodules-src" "true" $DEPS_FILE $REQUIREMENTS_FILE $PATH_FILE

    _log1 "download marketplace modules"
    _download_marketplace_modules "/odoo/marketplace-src"

    _log1 'concatenate requirements and deps for /odoo/marketplace-src'
    echo "/odoo/marketplace-src" >> $PATH_FILE
    _scan_dir_for_modules_and_install_requirements "/odoo/marketplace-src" "true" $DEPS_FILE $REQUIREMENTS_FILE $PATH_FILE

    _log1 'concatenate oca deps to /odoo/external-src/oca_dependencies.txt'
    cat $DEPS_FILE >> /odoo/external-src/oca_dependencies.txt

    _log1 'checkout OCA and GIT repos'
    python /odoo/auto_addons/addons.py

    _log1 'concatenate requirements and deps for /odoo/external-src'
    _scan_dir_for_modules_and_install_requirements "/odoo/external-src" "false" "" $REQUIREMENTS_FILE ""

    _log1 "install pip deps for $REQUIREMENTS_FILE"
    pip install -r $REQUIREMENTS_FILE

    _log1 "Replace ADDONS_PATH in /templates/odoo.cfg.tmpl"
    ADDONS_PATH=`cat /odoo/.addons_path`
    sed -i "s#__ADDONS_PATH__#$ADDONS_PATH#g" /templates/odoo.cfg.tmpl

    _log1 "Path:"
    _log1 $ADDONS_PATH

    #_purge
}

#RUN 
_intro
_build
_outro